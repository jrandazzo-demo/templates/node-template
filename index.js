// index.js

/**
 * Required External Modules
 */

const express = require("express");
const path = require("path");




/**
 * App Variables
 */

const app = express();
const port = process.env.PORT || "5000";


/**
 *  App Configuration
 */




app.set("css", path.join(__dirname, "css"));
app.set("layouts", path.join(__dirname, "layouts"));
app.set("components", path.join(__dirname, "components"));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use("/public", express.static('public'));

/**
 * Routes Definitions
 */

app.get("/", (req, res) => {
    res.render("sign-in", { title: "Home" });
  });

/**
 * Server Activation
 */

app.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
  });
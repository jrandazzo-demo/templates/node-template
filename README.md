# NodeApp

This node application demonstrates various usecases including SCM, CICD, Merge Requests, AutoDevOps, and more.



## Usage



## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[Apache-2.0](https://opensource.org/licenses/Apache-2.0)

## Thank you
[PUG-Bootstrap](https://github.com/rajasegar/PUG-Bootstrap)